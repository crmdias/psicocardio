package com.fillmandias.psicocardio;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Swelling#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Swelling extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    SymptomResult partialResult = new SymptomResult();



    private OnFragmentInteractionListener mListener;

    public Swelling() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Swelling.
     */
    // TODO: Rename and change types and number of parameters
    public static Swelling newInstance(String param1, String param2) {
        Swelling fragment = new Swelling();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    private class swellingActivityOnClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            int wellness = 0;
            if(((RadioButton) getView().findViewById(R.id.swellingNo)).isChecked())
                wellness = 0;
            else {
                RadioGroup g = (RadioGroup) getView().findViewById(R.id.swellingQuestionRadioGroup);
                int selectedID = g.getCheckedRadioButtonId();
                RadioButton b = (RadioButton) getView().findViewById(selectedID);
                wellness = g.indexOfChild(b);
            }

            partialResult.setWellness(wellness);

            if (mListener != null) {
                mListener.onFragmentFinish(partialResult);
            }
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_swelling, container, false);

        Button b = (Button) v.findViewById(R.id.swellingNextButton);
        b.setOnClickListener(new swellingActivityOnClickListener());
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
