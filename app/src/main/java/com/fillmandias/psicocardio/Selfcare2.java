package com.fillmandias.psicocardio;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Selfcare2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Selfcare2 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    SymptomResult partialResult = new SymptomResult();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Selfcare2() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Selfcare2.
     */
    // TODO: Rename and change types and number of parameters
    public static Selfcare2 newInstance(String param1, String param2) {
        Selfcare2 fragment = new Selfcare2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_selfcare2, container, false);

        Button b = (Button) v.findViewById(R.id.selfCare2Next);
        b.setOnClickListener(new selfCare2NextOnClickListener());
        return v;
    }

    private class selfCare2NextOnClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            int auxscore = 0;
            SeekBar b = (SeekBar) getView().findViewById(R.id.seekBarQ1);
            auxscore += b.getProgress();
            b = (SeekBar) getView().findViewById(R.id.seekBarQ2);
            auxscore += b.getProgress();
            b = (SeekBar) getView().findViewById(R.id.seekBarQ3);
            auxscore += b.getProgress();

            partialResult.setSelfcare(auxscore);

            if (mListener != null) {
                mListener.onFragmentFinish(partialResult);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
