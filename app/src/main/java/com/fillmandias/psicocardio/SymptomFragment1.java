package com.fillmandias.psicocardio;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Date;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SymptomFragment1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SymptomFragment1 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    SymptomResult partialResult = new SymptomResult();
    private Date selectedDate;

    // TODO: Rename and change types of parameters


    private OnFragmentInteractionListener mListener;


    public SymptomFragment1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SymptomFragment1.
     */
    // TODO: Rename and change types and number of parameters
    public static SymptomFragment1 newInstance(String param1, String param2) {
        SymptomFragment1 fragment = new SymptomFragment1();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        Calendar c = Calendar.getInstance();
        selectedDate = c.getTime();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_symptom_fragment1, container, false);

        Button b = (Button) v.findViewById(R.id.fragment1NextButton);
        b.setOnClickListener(new startSymptomActivityOnClickListener());
        TextView t = (TextView) v.findViewById(R.id.dateEditText);
        t.setOnClickListener(new MyEditTextDatePicker(getContext(), (EditText) v.findViewById(R.id.dateEditText)));
        return v;
    }

    private class startSymptomActivityOnClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            partialResult.setHeartRate(Integer.parseInt(((TextView)getView().findViewById(R.id.pulseEditText)).getText().toString()));
            partialResult.setWeight(Integer.parseInt(((TextView)getView().findViewById(R.id.weightEditText)).getText().toString()));
            partialResult.setBloodPressure1(Integer.parseInt(((TextView)getView().findViewById(R.id.pressureEditText1)).getText().toString()));
            partialResult.setBloodPressure2(Integer.parseInt(((TextView)getView().findViewById(R.id.pressureEditText2)).getText().toString()));
            partialResult.setWellness(((SeekBar)getView().findViewById(R.id.wellnessSeekBar)).getProgress());
            partialResult.setDate(selectedDate);

            if (mListener != null) {
                mListener.onFragmentFinish(partialResult);
            }
        }
    }

    public class MyEditTextDatePicker  implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
        EditText _editText;
        private int _day;
        private int _month;
        private int _birthYear;
        private Context _context;

        public MyEditTextDatePicker(Context context, EditText editText)
        {
            Activity act = (Activity)context;
            this._editText = editText;
            this._editText.setOnClickListener(this);
            this._context = context;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            _birthYear = year;
            _month = monthOfYear;
            _day = dayOfMonth;
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, monthOfYear);
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            selectedDate = c.getTime();
            updateDisplay();
        }
        @Override
        public void onClick(View v) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

            DatePickerDialog dialog = new DatePickerDialog(_context, this,
                    calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH));
            dialog.show();

        }

        // updates the date in the birth date EditText
        private void updateDisplay() {

            _editText.setText(new StringBuilder()
                    // Month is 0 based so add 1
                    .append(_day).append("/").append(_month + 1).append("/").append(_birthYear).append(" "));
        }
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
