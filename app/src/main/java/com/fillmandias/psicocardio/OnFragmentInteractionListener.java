package com.fillmandias.psicocardio;

/**
 * Created by crmdias on 05/12/2016.
 */

public interface OnFragmentInteractionListener {
    void onFragmentFinish(SymptomResult result);
}
