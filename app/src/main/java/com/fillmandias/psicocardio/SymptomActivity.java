package com.fillmandias.psicocardio;

import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import com.sendgrid.*;

import java.io.IOException;
import java.util.Date;

public class SymptomActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    enum FragmentCallbacks  {SymptomFragment1, BasicSymptom, Swelling, Tiredness, SelfCare1, SelfCare2, Adhesion1, Adhesion2, Adhesion3, Adhesion4, Adhesion5, Adhesion6}
    SymptomResult result;
    FragmentCallbacks currentFragmentType;
    Fragment currentFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symptom);

        this.result = new SymptomResult();

        // add rowLayout to the root layout somewhere here

        if(currentFragment == null) {
            Fragment myFrag = new SymptomFragment1();
            FragmentManager fragMan = getSupportFragmentManager();
            FragmentTransaction fragTransaction = fragMan.beginTransaction();

            fragTransaction.add(R.id.activity_symptom, myFrag, "Symptom Fragment 1").commit();
            currentFragmentType = FragmentCallbacks.SymptomFragment1;
            currentFragment = myFrag;
        }
    }

        public void onFragmentFinish(SymptomResult partialResult) {
            //partial data retrieved from a fragment.
            //we have to attribute this partial data to our final result
            //and exchange the current fragment for the next fragment

            switch(currentFragmentType)
            {
                case SymptomFragment1:
                {
                    this.result.setWeight(partialResult.getWeight());
                    this.result.setDate(partialResult.getDate());
                    this.result.setBloodPressure1(partialResult.getBloodPressure1());
                    this.result.setBloodPressure2(partialResult.getBloodPressure2());
                    this.result.setHeartRate(partialResult.getHeartRate());
                    this.result.setWellness(partialResult.getWellness());

                    FragmentManager fragMan = getSupportFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();

                    fragTransaction.remove(currentFragment).commit();
                    fragTransaction = fragMan.beginTransaction();

                    Fragment myFrag = new BasicSymptomData();
                    fragTransaction.add(R.id.activity_symptom, myFrag , "Sintomas Básicos").commit();

                    currentFragmentType = FragmentCallbacks.BasicSymptom;
                    currentFragment = myFrag;

                }break;

                case BasicSymptom:
                {
                    //TODO: set basicsymptom things

                    FragmentManager fragMan = getSupportFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();

                    fragTransaction.remove(currentFragment).commit();
                    fragTransaction = fragMan.beginTransaction();

                    Fragment myFrag = new Swelling();
                    fragTransaction.add(R.id.activity_symptom, myFrag , "Inchaço").commit();

                    currentFragmentType = FragmentCallbacks.Swelling;
                    currentFragment = myFrag;

                }break;

                case Swelling:
                {
                    this.result.setSwelling(partialResult.getSwelling());

                    FragmentManager fragMan = getSupportFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();

                    // add rowLayout to the root layout somewhere here

                    fragTransaction.remove(currentFragment).commit();
                    fragTransaction = fragMan.beginTransaction();

                    Fragment myFrag = new Tiredness();
                    fragTransaction.add(R.id.activity_symptom, myFrag , "Cansaço").commit();
                    currentFragmentType = FragmentCallbacks.Tiredness;
                    currentFragment = myFrag;


                }break;

                case Tiredness:
                {
                    this.result.setTirednessFrequency(partialResult.getTirednessFrequency());
                    this.result.setTiredness(partialResult.getTiredness());

                    FragmentManager fragMan = getSupportFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();

                    // add rowLayout to the root layout somewhere here

                    fragTransaction.remove(currentFragment).commit();
                    fragTransaction = fragMan.beginTransaction();

                    Fragment myFrag = new Selfcare1();
                    fragTransaction.add(R.id.activity_symptom, myFrag , "Cuidado próprio").commit();
                    currentFragmentType = FragmentCallbacks.SelfCare1;
                    currentFragment = myFrag;

                }break;

                case SelfCare1:
                {
                    this.result.setSelfcare(partialResult.getSelfcare());
                    FragmentManager fragMan = getSupportFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();

                    // add rowLayout to the root layout somewhere here

                    fragTransaction.remove(currentFragment).commit();
                    fragTransaction = fragMan.beginTransaction();

                    Fragment myFrag = new Selfcare2();
                    fragTransaction.add(R.id.activity_symptom, myFrag , "Cuidado próprio").commit();
                    currentFragmentType = FragmentCallbacks.SelfCare2;
                    currentFragment = myFrag;

                }break;

                case SelfCare2:
                {
                    this.result.setSelfcare(this.result.getSelfcare() + partialResult.getSelfcare());

                    FragmentManager fragMan = getSupportFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();

                    // add rowLayout to the root layout somewhere here

                    fragTransaction.remove(currentFragment).commit();
                    fragTransaction = fragMan.beginTransaction();

                    Fragment myFrag = new Adhesion1();
                    fragTransaction.add(R.id.activity_symptom, myFrag , "Adesão").commit();
                    currentFragmentType = FragmentCallbacks.Adhesion1;
                    currentFragment = myFrag;

                }break;

                case Adhesion1:
                {
                    this.result.setAdhesion(this.result.getAdhesion() + partialResult.getAdhesion());

                    FragmentManager fragMan = getSupportFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();

                    // add rowLayout to the root layout somewhere here

                    fragTransaction.remove(currentFragment).commit();
                    fragTransaction = fragMan.beginTransaction();

                    Fragment myFrag = new Adhesion2();
                    fragTransaction.add(R.id.activity_symptom, myFrag , "Adesão").commit();
                    currentFragmentType = FragmentCallbacks.Adhesion2;
                    currentFragment = myFrag;

                }break;

                case Adhesion2:
                {
                    this.result.setAdhesion(this.result.getAdhesion() + partialResult.getAdhesion());

                    FragmentManager fragMan = getSupportFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();

                    // add rowLayout to the root layout somewhere here

                    fragTransaction.remove(currentFragment).commit();
                    fragTransaction = fragMan.beginTransaction();

                    Fragment myFrag = new Adhesion3();
                    fragTransaction.add(R.id.activity_symptom, myFrag , "Adesão").commit();
                    currentFragmentType = FragmentCallbacks.Adhesion3;
                    currentFragment = myFrag;

                }break;

                case Adhesion3:
                {
                    this.result.setAdhesion(this.result.getAdhesion() + partialResult.getAdhesion());

                    FragmentManager fragMan = getSupportFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();

                    // add rowLayout to the root layout somewhere here

                    fragTransaction.remove(currentFragment).commit();
                    fragTransaction = fragMan.beginTransaction();

                    Fragment myFrag = new Adhesion4();
                    fragTransaction.add(R.id.activity_symptom, myFrag , "Adesão").commit();
                    currentFragmentType = FragmentCallbacks.Adhesion4;
                    currentFragment = myFrag;

                }break;

                case Adhesion4:
                {
                    this.result.setAdhesion(this.result.getAdhesion() + partialResult.getAdhesion());

                    FragmentManager fragMan = getSupportFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();

                    // add rowLayout to the root layout somewhere here

                    fragTransaction.remove(currentFragment).commit();
                    fragTransaction = fragMan.beginTransaction();

                    Fragment myFrag = new Adhesion5();
                    fragTransaction.add(R.id.activity_symptom, myFrag , "Adesão").commit();
                    currentFragmentType = FragmentCallbacks.Adhesion5;
                    currentFragment = myFrag;

                }break;

                case Adhesion5:
                {
                    this.result.setAdhesion(this.result.getAdhesion() + partialResult.getAdhesion());

                    FragmentManager fragMan = getSupportFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();

                    // add rowLayout to the root layout somewhere here

                    fragTransaction.remove(currentFragment).commit();
                    fragTransaction = fragMan.beginTransaction();

                    Fragment myFrag = new Adhesion6();
                    fragTransaction.add(R.id.activity_symptom, myFrag , "Adesão").commit();
                    currentFragmentType = FragmentCallbacks.Adhesion6;
                    currentFragment = myFrag;

                }break;

                case Adhesion6:
                {
                    this.result.setAdhesion(this.result.getAdhesion() + partialResult.getAdhesion());

                    FragmentManager fragMan = getSupportFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();

                    // add rowLayout to the root layout somewhere here

                    fragTransaction.remove(currentFragment).commit();

                    //TODO: Do Something with result AKA - > send to email - > save DB

                    try {
                        /**** Connect to MongoDB ****/
                        // Since 2.10.0, uses MongoClient
                        MongoClientURI uri  = new MongoClientURI("mongodb://ds047950.mlab.com/todo");
                        MongoClient mongo = new MongoClient(uri);

                        /**** Get database ****/
                        // if database doesn't exists, MongoDB will create it for you
                        MongoDatabase db = mongo.getDatabase(uri.getDatabase());

                        /**** Get collection / table from 'testdb' ****/
                        // if collection doesn't exists, MongoDB will create it for you
                        MongoCollection table = db.getCollection("patientLogs");

                        /**** Insert ****/
                        // create a document to store key and value
                        DBObject dbObject = (DBObject) JSON.parse(result.getJson());

                        table.insertOne(dbObject);
                    }
                    catch (Throwable t)
                    {//TODO: do something

                    }
                    /*** database interaction finished ***/

                    try {
                        Email from = new Email("cuidados@heartcare.com");
                        String subject = "HeartCare - Novo relatório de sintomas";
                        Email to = new Email("cristianormdias@gmail.com");
                        Content content = new Content("text/plain", "Segue abaixo informações do relatório:\n\n" + result.getJson());
                        Mail mail = new Mail(from, subject, to, content);

                        SendGrid sg = new SendGrid("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");
                        Request request = new Request();
                        try {
                            request.method = Method.POST;
                            request.endpoint = "mail/send";
                            request.body = mail.build();
                            Response response = sg.api(request);
                            System.out.println(response.statusCode);
                            System.out.println(response.body);
                            System.out.println(response.headers);
                        } catch (IOException ex) {
                        }
                    }
                    catch(Throwable t)
                    {
                    //TODO: Actually do something
                    }

                    AlertDialog alertDialog = new AlertDialog.Builder(SymptomActivity.this).create();
                    alertDialog.setTitle("Sucesso");
                    alertDialog.setMessage("Informações enviadas com sucesso para a equipe médica.");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                    finish();

                }break;
            }
        }


}
