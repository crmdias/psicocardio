package com.fillmandias.psicocardio;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BasicSymptomData#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BasicSymptomData extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    SymptomResult partialResult = new SymptomResult();


    private OnFragmentInteractionListener mListener;

    public BasicSymptomData() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BasicSymptomData.
     */
    // TODO: Rename and change types and number of parameters
    public static BasicSymptomData newInstance(String param1, String param2) {
        BasicSymptomData fragment = new BasicSymptomData();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_basic_symptom_data, container, false);

        Button b = (Button) v.findViewById(R.id.basicSymptomNext);
        b.setOnClickListener(new basicSymptomDataNextOnClickListener());
        return v;
    }

    private class basicSymptomDataNextOnClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            //TODO: set actual partial result values



            if (mListener != null) {
                mListener.onFragmentFinish(partialResult);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
