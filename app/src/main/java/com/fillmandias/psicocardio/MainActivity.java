package com.fillmandias.psicocardio;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b = (Button) findViewById(R.id.button2);
        b.setOnClickListener(new startSymptomActivityOnClickListener());
    }



    private class startSymptomActivityOnClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View view)
        {
            SymptomActivity a = new SymptomActivity();
            Intent i = new Intent(MainActivity.this, SymptomActivity.class);
            startActivity(i);
            //TODO: Notify action OK
        }
    }

}
