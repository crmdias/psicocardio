package com.fillmandias.psicocardio;

import java.util.Date;
import com.google.gson.Gson;

/**
 * Created by Bruno on 05/12/2016.
 */

public class SymptomResult {
    private Date date;
    private int weight;
    private int bloodPressure1;
    private int bloodPressure2;
    private int heartRate;
    private int wellness;
    private int dayBreathing;
    private int nightBreathing;
    private int swelling;
    private int tiredness;
    private int selfcare;
    private int adhesion;
    private int pillsUse;
    private int tirednessFrequency;

    public SymptomResult()
    {}

    public SymptomResult(int weight, int bloodPressure1, int bloodPressure2, int heartRate, int wellness, Date date,
                         int dayBreathing, int nightBreathing, boolean dayBreathingR, boolean nightBreathingR,
                         int tiredness, int swelling, int selfcare, int pillsUse, int tirednessFrequency, int adhesion) {
        this.weight = weight;
        this.date = date;
        this.bloodPressure1 = bloodPressure1;
        this.bloodPressure2 = bloodPressure2;
        this.heartRate = heartRate;
        this.wellness = wellness;
        this.dayBreathing = dayBreathing;
        this.nightBreathing = nightBreathing;
        this.swelling = swelling;
        this.tiredness = tiredness;
        this.selfcare = selfcare;
        this.pillsUse = pillsUse;
        this.tirednessFrequency = tirednessFrequency;
        this.adhesion = adhesion;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getBloodPressure1() {
        return bloodPressure1;
    }
    public int getAdhesion() {
        return adhesion;
    }

    public void setAdhesion(int adhesion) {
        this.adhesion = adhesion;
    }
    public void setBloodPressure1(int bloodPressure1) {
        this.bloodPressure1 = bloodPressure1;
    }

    public int getBloodPressure2() {
        return bloodPressure2;
    }

    public void setBloodPressure2(int bloodPressure2) {
        this.bloodPressure2 = bloodPressure2;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public int getWellness() {
        return wellness;
    }

    public void setWellness(int wellness) {
        this.wellness = wellness;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDayBreathing() {
        return dayBreathing;
    }

    public void setDayBreathing(int dayBreathing) {
        this.dayBreathing = dayBreathing;
    }

    public int getNightBreathing() {
        return nightBreathing;
    }

    public void setNightBreathing(int nightBreathing) {
        this.nightBreathing = nightBreathing;
    }

    public int getSwelling() {
        return swelling;
    }

    public void setSwelling(int swelling) {
        this.swelling = swelling;
    }

    public int getTiredness() {
        return tiredness;
    }

    public void setTiredness(int tiredness) {
        this.tiredness = tiredness;
    }

    public int getSelfcare() {
        return selfcare;
    }

    public void setSelfcare(int selfcare) {
        this.selfcare = selfcare;
    }

    public int getPillsUse() {
        return pillsUse;
    }

    public void setPillsUse(int pillsUse) {
        this.pillsUse = pillsUse;
    }


    public int getTirednessFrequency() {
        return tirednessFrequency;
    }

    public void setTirednessFrequency(int tirednessFrequency) {
        this.tirednessFrequency = tirednessFrequency;
    }

    public String getJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
