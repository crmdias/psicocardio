package com.fillmandias.psicocardio;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Adhesion1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Adhesion1 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    SymptomResult partialResult = new SymptomResult();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Adhesion1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Selfcare1.
     */
    // TODO: Rename and change types and number of parameters
    public static Adhesion1 newInstance(String param1, String param2) {
        Adhesion1 fragment = new Adhesion1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_adhesion1, container, false);

        Button b = (Button) v.findViewById(R.id.adhesion1Next);
        b.setOnClickListener(new selfCare1NextOnClickListener());
        return v;
    }

    private class selfCare1NextOnClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            int adhesion = 0;
            RadioGroup g = (RadioGroup) getView().findViewById(R.id.adhesion1RadioGroup);
            int selectedID = g.getCheckedRadioButtonId();
            RadioButton b = (RadioButton) getView().findViewById(selectedID);
            adhesion = g.indexOfChild(b);


            partialResult.setAdhesion(adhesion);

            if (mListener != null) {
                mListener.onFragmentFinish(partialResult);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
